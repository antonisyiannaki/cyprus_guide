package com.example.antonis.mygraphics;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.antonis.cyprusguide.R;

public class Ayia_Napa extends AppCompatActivity
{

        @Override
        protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ayianapa);
    }

    public void onnissiClick (View v)
    {
        if (v.getId() == R.id.imageButton18)
        {
            Intent a = new Intent(Ayia_Napa.this, AyiaNapa_Nissi_Beach.class);
            startActivity(a);
        }
    }

    public void onclubClick (View v)
    {
        if (v.getId() == R.id.imageButton17)
        {
            Intent a = new Intent(Ayia_Napa.this, AyiaNapa_Clubs.class);
            startActivity(a);
        }
    }

    public void onmakronisosClick (View v)
    {
        if (v.getId() == R.id.imageButton15)
        {
            Intent a = new Intent(Ayia_Napa.this, AyiaNapa_Makronisos_Beach.class);
            startActivity(a);
        }
    }

    public void onwaterparkClick (View v)
    {
        if (v.getId() == R.id.imageButton16)
        {
            Intent a = new Intent(Ayia_Napa.this, AyiaNapa_WaterWorld_Themed_Waterpark.class);
            startActivity(a);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                Intent i = new Intent(this, Settings.class);
                startActivity(i);
                return true;

            case R.id.homepage:
                Intent a = new Intent(this, Home_Page.class);
                startActivity(a);
                return true;

            case R.id.logout:
                Intent g = new Intent(this, Sign_In.class);
                startActivity(g);
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }
}
