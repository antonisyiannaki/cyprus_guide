package com.example.antonis.mygraphics;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.antonis.cyprusguide.R;

public class Larnaca extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_larnaca);
    }

    public void onmakenzieClick (View v)
    {
        if (v.getId() == R.id.makenzie)
        {
            Intent a = new Intent(Larnaca.this, Larnaca_Mackenzie.class);
            startActivity(a);
        }
    }

    public void onkamaresClick (View v)
    {
        if (v.getId() == R.id.kamares)
        {
            Intent a = new Intent(Larnaca.this, Larnaca_Kamares.class);
            startActivity(a);
        }
    }

    public void onfinikoudesClick (View v)
    {
        if (v.getId() == R.id.finikoudes)
        {
            Intent a = new Intent(Larnaca.this, Larnaca_Finikoudes.class);
            startActivity(a);
        }
    }

    public void onkitionClick (View v)
    {
        if (v.getId() == R.id.kition)
        {
            Intent a = new Intent(Larnaca.this, Larnaca_Kition.class);
            startActivity(a);
        }
    }

    public void onlazarosClick (View v)
    {
        if (v.getId() == R.id.lazaros)
        {
            Intent a = new Intent(Larnaca.this, Larnaca_AgiosLazaros.class);
            startActivity(a);
        }
    }

    public void onairportClick (View v)
    {
        if (v.getId() == R.id.airport)
        {
            Intent a = new Intent(Larnaca.this, LarnacaInternationalAirport.class);
            startActivity(a);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                Intent i = new Intent(this, Settings.class);
                startActivity(i);
                return true;

            case R.id.homepage:
                Intent a = new Intent(this, Home_Page.class);
                startActivity(a);
                return true;

            case R.id.logout:
                Intent g = new Intent(this, Sign_In.class);
                startActivity(g);
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }
}
