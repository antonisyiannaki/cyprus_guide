package com.example.antonis.mygraphics;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.antonis.cyprusguide.R;

public class Nicosia extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nicosia);
    }

    public void onmallClick (View v)
    {
        if (v.getId() == R.id.mall)
        {
            Intent a = new Intent(Nicosia.this, Nicosia_Mall_of_Cyprus.class);
            startActivity(a);
        }
    }

    public void onwallsClick (View v)
    {
        if (v.getId() == R.id.walls)
        {
            Intent a = new Intent(Nicosia.this, Nicosia_Walls.class);
            startActivity(a);
        }
    }

    public void onmonasteryClick (View v)
    {
        if (v.getId() == R.id.monastery)
        {
            Intent a = new Intent(Nicosia.this, Nicosia_Kykkos_Monastery.class);
            startActivity(a);
        }
    }

    public void ontowerClick (View v)
    {
        if (v.getId() == R.id.tower)
        {
            Intent a = new Intent(Nicosia.this, Nicosia_Shacolas_Tower.class);
            startActivity(a);
        }
    }

    public void onarchiepiskopiClick (View v)
    {
        if (v.getId() == R.id.archiepiskopi)
        {
            Intent a = new Intent(Nicosia.this, Nicosia_Archiepiskopi.class);
            startActivity(a);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                Intent i = new Intent(this, Settings.class);
                startActivity(i);
                return true;

            case R.id.homepage:
                Intent a = new Intent(this, Home_Page.class);
                startActivity(a);
                return true;

            case R.id.logout:
                Intent g = new Intent(this, Sign_In.class);
                startActivity(g);
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }
}
