package com.example.antonis.mygraphics;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.antonis.cyprusguide.R;

public class Home_Page extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);
    }

    public void onNicosiaClick (View v)
    {
        if (v.getId() == R.id.button8)
        {
            Intent a = new Intent(Home_Page.this, Nicosia.class);
            startActivity(a);
        }
    }

    public void onLarnacaClick (View v)
    {
        if (v.getId() == R.id.button9)
        {
            Intent a = new Intent(Home_Page.this, Larnaca.class);
            startActivity(a);
        }
    }

    public void onLimassolClick (View v)
    {
        if (v.getId() == R.id.button10)
        {
            Intent a = new Intent(Home_Page.this, Limassol.class);
            startActivity(a);
        }
    }

    public void onPaphosClick (View v)
    {
        if (v.getId() == R.id.button11)
        {
            Intent a = new Intent(Home_Page.this, Paphos.class);
            startActivity(a);
        }
    }

    public void onProtarasClick (View v)
    {
        if (v.getId() == R.id.button12)
        {
            Intent a = new Intent(Home_Page.this, Protaras_Paralimni.class);
            startActivity(a);
        }
    }

    public void onAyiaNapaClick (View v)
    {
        if (v.getId() == R.id.button13)
        {
            Intent a = new Intent(Home_Page.this, Ayia_Napa.class);
            startActivity(a);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                Intent i = new Intent(this, Settings.class);
                startActivity(i);
                return true;

            case R.id.homepage:
                Intent a = new Intent(this, Home_Page.class);
                startActivity(a);
                return true;

            case R.id.logout:
                Intent g = new Intent(this, Sign_In.class);
                startActivity(g);
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }
}
