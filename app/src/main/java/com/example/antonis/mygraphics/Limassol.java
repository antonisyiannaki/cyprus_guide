package com.example.antonis.mygraphics;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.antonis.cyprusguide.R;

public class Limassol extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_limassol);
    }

    public void onmarinaClick (View v)
    {
        if (v.getId() == R.id.imageButton5)
        {
            Intent a = new Intent(Limassol.this, Limassol_Marina.class);
            startActivity(a);
        }
    }

    public void onportClick (View v)
    {
        if (v.getId() == R.id.imageButton1)
        {
            Intent a = new Intent(Limassol.this, Limassol_Port.class);
            startActivity(a);
        }
    }

    public void onfasouriClick (View v)
    {
        if (v.getId() == R.id.imageButton2)
        {
            Intent a = new Intent(Limassol.this, Limassol_Fassouri_Watermania.class);
            startActivity(a);
        }
    }

    public void onkolossiClick (View v)
    {
        if (v.getId() == R.id.imageButton3)
        {
            Intent a = new Intent(Limassol.this, Limassol_Kolossi.class);
            startActivity(a);
        }
    }

    public void onamuseumClick (View v)
    {
        if (v.getId() == R.id.imageButton4)
        {
            Intent a = new Intent(Limassol.this, Limassol_Archeological_Museum.class);
            startActivity(a);
        }
    }

    public void ontmuseumClick (View v)
    {
        if (v.getId() == R.id.imageButton5)
        {
            Intent a = new Intent(Limassol.this, Limassol_Theatre_Museum.class);
            startActivity(a);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                Intent i = new Intent(this, Settings.class);
                startActivity(i);
                return true;

            case R.id.homepage:
                Intent a = new Intent(this, Home_Page.class);
                startActivity(a);
                return true;

            case R.id.logout:
                Intent g = new Intent(this, Sign_In.class);
                startActivity(g);
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }
}
