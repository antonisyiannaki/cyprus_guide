package com.example.antonis.mygraphics;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.antonis.cyprusguide.R;
import com.facebook.CallbackManager;

public class Sign_In extends Activity {

    private EditText editTextUsername;
    private EditText editTextPassword;
    public static final String NAME_KEY = "NAME_KEY";
    public static final String PASSWORD_KEY = "PASSWORD_KEY";
    private SharedPreferences sharedPreferences;

    Database helper = new Database(this);
    Button btnRegister;
   // LoginButton login_button;
    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        init();

        editTextUsername = (EditText) findViewById(R.id.username);
        editTextPassword = (EditText) findViewById(R.id.password);
        sharedPreferences = getSharedPreferences("MySharedPreMain", Context.MODE_PRIVATE);

        if (sharedPreferences.contains(NAME_KEY)) {
            editTextUsername.setText(sharedPreferences.getString(NAME_KEY, ""));
        }

        if (sharedPreferences.contains(PASSWORD_KEY)) {
            editTextPassword.setText(sharedPreferences.getString(PASSWORD_KEY, ""));
        }
    }

    public void save(View v){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(NAME_KEY, editTextUsername.getText().toString());
        editor.putString(PASSWORD_KEY, editTextPassword.getText().toString());
        editor.commit();
        Toast.makeText(v.getContext(),"data saved",Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void onLogInClick(View v)
    {
        if (v.getId() == R.id.btnLogIn) {
            EditText a = (EditText) findViewById(R.id.username);
            String str = a.getText().toString();
            EditText b = (EditText) findViewById(R.id.password);
            String pass = b.getText().toString();

            String password = helper.searchPass(str);
            if (pass.equals(password))
            {
                Intent i = new Intent(Sign_In.this, Home_Page.class);
                startActivity(i);
            }
            else
            {
                Toast temp = Toast.makeText(Sign_In.this, "Username and password doesn't match!" , Toast.LENGTH_SHORT);
                temp.show();
            }
        }


    }

    public void init() {
        btnRegister = (Button) findViewById(R.id.button2);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent homeIntent = new Intent(Sign_In.this, Sign_Up.class);
                startActivity(homeIntent);
            }
        });
    }
}
