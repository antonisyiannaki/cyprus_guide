package com.example.antonis.mygraphics;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.antonis.cyprusguide.R;

public class Sign_Up extends AppCompatActivity {

    static final int REQUEST_IMAGE_CAPTURE = 1;

    Database helper = new Database(this);
    Button btn_photo;
    ImageView iv_photo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        btn_photo = (Button) findViewById (R.id.btn_photo);
        iv_photo = (ImageView) findViewById(R.id.iv_photo);

        if(!hasCamera())
        {
            btn_photo.setEnabled(false);
        }

        btn_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
            }
        });
    }

    private boolean hasCamera()
    {
        return getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK)
        {
            Bundle extras = data.getExtras();
            Bitmap photo = (Bitmap) extras.get("data");
            iv_photo.setImageBitmap(photo);
        }
    }

    public void onRegisterClick(View v) {
        if (v.getId() == R.id.btnSignUp) {
            Log.d("xxx", "xxx");
            EditText name = (EditText) findViewById(R.id.etName);
            EditText surname = (EditText) findViewById(R.id.etSurname);
            EditText username = (EditText) findViewById(R.id.etUsername);
            EditText password = (EditText) findViewById(R.id.etPassword);
            EditText confirm_password = (EditText) findViewById(R.id.etComPassword);

            String namestr = name.getText().toString();
            String surnamestr = surname.getText().toString();
            String usernamestr = username.getText().toString();
            String passwordstr = password.getText().toString();
            String confpasswordstr = confirm_password.getText().toString();

            if (!passwordstr.equals(confpasswordstr)) {
                Log.d("xxx", "yyy");

                //this is code that it will shows when two passwords the user enters, don't match
                Toast passcode = Toast.makeText(Sign_Up.this, "Password doesn't match!" + !password.equals(confirm_password), Toast.LENGTH_SHORT);
                passcode.show();

            } else {

                // insert the information to database
                Contacts c = new Contacts();
                c.setName(namestr);
                c.setSurname(surnamestr);
                c.setUsername(usernamestr);
                c.setPassword(passwordstr);

                helper.insertContact(c);

                Intent homeIntent = new Intent(Sign_Up.this, Sign_In.class);
                startActivity(homeIntent);

            }
        }
    }
}
