package com.example.antonis.mygraphics;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.antonis.cyprusguide.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;

public class Limassol_Fassouri_Watermania extends AppCompatActivity implements OnMapReadyCallback {

    GoogleMap mGoogleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (googleServicesAvailable())
        {
            Toast.makeText(this, "Fassouri_Watermania",Toast.LENGTH_LONG).show();
            setContentView(R.layout.activity_fassouri);
            initMap();
        }
    }

    private void initMap()
    {
        MapFragment mfragment = (MapFragment) getFragmentManager().findFragmentById(R.id.myFragment6);
        mfragment.getMapAsync(this);
    }

    public boolean googleServicesAvailable()
    {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int isAvailable = api.isGooglePlayServicesAvailable(this);
        if (isAvailable == ConnectionResult.SUCCESS)
        {
            return true;
        }
        else if (api.isUserResolvableError(isAvailable))
        {
            Dialog dialog = api.getErrorDialog(this, isAvailable,0);
            dialog.show();
        }else {
            Toast.makeText(this, "Can't connect to play services",Toast.LENGTH_LONG).show();
        }
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mGoogleMap = googleMap;
        goToLocationZoom(34.652345,32.9734987, 15);
    }

    public void goToLocation(double lat, double lng)
    {
        LatLng ll = new LatLng(lat, lng);
        CameraUpdate update = CameraUpdateFactory.newLatLng(ll);
        mGoogleMap.moveCamera(update);
    }

    public void goToLocationZoom(double lat, double lng, float zoom)
    {
        LatLng ll = new LatLng(lat, lng);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, zoom);
        mGoogleMap.moveCamera(update);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                Intent i = new Intent(this, Settings.class);
                startActivity(i);
                return true;

            case R.id.homepage:
                Intent a = new Intent(this, Home_Page.class);
                startActivity(a);
                return true;

            case R.id.logout:
                Intent g = new Intent(this, Sign_In.class);
                startActivity(g);
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }
}