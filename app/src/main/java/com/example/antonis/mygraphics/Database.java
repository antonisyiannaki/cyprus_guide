package com.example.antonis.mygraphics;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Database extends SQLiteOpenHelper {

    private static final int DATEBASE_VERSION=1;
    private static final String DATABASE_NAME="contacts.db";
    private static final String TABLE_NAME="contacts";
    private static final String COLUMN_ID="id";
    private static final String COLUMN_NAME="name";
    private static final String COLUMN_SURNAME="surname";
    private static final String COLUMN_USERNAME="username";
    private static final String COLUMN_PASSWORD="password";
    SQLiteDatabase db;
    private static final String CREATE_TABLE="create table contacts (id integer primary key not null , " +
            "name text not null, surname text not null, username text not null, password text not null);";

    public Database (Context context)
    {

        super(context, DATABASE_NAME, null, DATEBASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
//        db.execSQL("CREATE TABLE");
        db.execSQL(CREATE_TABLE);
        this.db=db;

    }

    public void insertContact(Contacts c){

        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        String query = "select * from contacts";
        Cursor cursor = db.rawQuery(query, null);
        int count= cursor.getCount();

        values.put(COLUMN_ID, count);
        values.put(COLUMN_NAME, c.getName());
        values.put(COLUMN_SURNAME, c.getSurname());
        values.put(COLUMN_USERNAME, c.getUsername());
        values.put(COLUMN_PASSWORD, c.getPassword());

        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public String searchPass(String username)
    {

        db= this.getReadableDatabase();
        String query = "select username , password from " +TABLE_NAME;
        Cursor cursor=db.rawQuery(query, null);
        String a, b;
        b = "not found";

        if (cursor.moveToFirst()){

            do {
                a=cursor.getString(0);

                if (a.equals(username)){
                    b=cursor.getString(1);
                    break;
                }
            }
            while (cursor.moveToNext());
        }

        return b;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){

        String query = "DROP IF TABLE EXISTS" + TABLE_NAME;
        db.execSQL(query);
        this.onCreate(db);
    }
}
