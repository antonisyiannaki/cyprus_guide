package com.example.antonis.mygraphics;

public class Contacts {

    String name;
    String surname;
    String username;
    String password;

    public void setName(String name) {

        this.name = name;
    }

    public String getName() {

        return this.name;
    }

    public void setSurname(String surname) {

        this.surname = surname;
    }

    public String getSurname() {

        return this.surname;
    }


    public void setUsername(String username) {

        this.username = username;
    }

    public String getUsername() {

        return this.username;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getPassword() {

        return this.password;
    }
}
