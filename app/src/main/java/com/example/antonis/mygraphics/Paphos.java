package com.example.antonis.mygraphics;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.antonis.cyprusguide.R;

public class Paphos extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paphos);
    }

    public void onpaphosairClick (View v)
    {
        if (v.getId() == R.id.imageButton13)
        {
            Intent a = new Intent(Paphos.this, PaphosInternationalAirport.class);
            startActivity(a);
        }
    }

    public void oncoralbayClick (View v)
    {
        if (v.getId() == R.id.imageButton12)
        {
            Intent a = new Intent(Paphos.this, Paphos_Coral_Bay.class);
            startActivity(a);
        }
    }

    public void onhouseClick (View v)
    {
        if (v.getId() == R.id.imageButton14)
        {
            Intent a = new Intent(Paphos.this, Paphos_House.class);
            startActivity(a);
        }
    }

    public void onsolomoniClick (View v)
    {
        if (v.getId() == R.id.imageButton15)
        {
            Intent a = new Intent(Paphos.this, Paphos_Solomoni.class);
            startActivity(a);
        }
    }

    public void onpetraClick (View v)
    {
        if (v.getId() == R.id.imageButton16)
        {
            Intent a = new Intent(Paphos.this, Paphos_PetraTouRomiou.class);
            startActivity(a);
        }
    }

    public void onchrysospiliwtissaClick (View v)
    {
        if (v.getId() == R.id.imageButton17)
        {
            Intent a = new Intent(Paphos.this, Paphos_Chrysospiliotissa.class);
            startActivity(a);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                Intent i = new Intent(this, Settings.class);
                startActivity(i);
                return true;

            case R.id.homepage:
                Intent a = new Intent(this, Home_Page.class);
                startActivity(a);
                return true;

            case R.id.logout:
                Intent g = new Intent(this, Sign_In.class);
                startActivity(g);
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }
}
